let navbar = $("#collapse-navbar");
let banner = $(".topCasaFina-Banner");
navbar.on("hidden.bs.collapse", function () {
  banner.css("transform", "translate(-50%,-50%)");
});
navbar.on("show.bs.collapse", function () {
  banner.css("transform", "translate(-50%,-10%)");
});
